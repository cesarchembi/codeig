var $baseUrl = $('div#base-url').html();

var app = angular.module('myApp', ['ngMessages']);

app.service('dataService', function($http) {
    this.getData = function(params, path, callbackFunc){
        $http({
            method: 'GET',
            url: $baseUrl + path,
            params: params
            /// headers: {'Authorization': 'Token token=xxxxYYYYZzzz'}
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

    this.setData = function(params, path, callbackFunc){
        $.ajax({
            method: 'POST',
            url: $baseUrl + path,
            data: params
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

});

app.controller('myController', ['$scope', 'dataService', function($scope, dataService){
  console.log({"Controlador": "myController services2"});

  $scope.showSave = false;
  $scope.errorSave = false;
  $scope.errorValid = false;
  $scope.departamentos = {};
  $scope.municipios = {};
  $scope.Formulario = {};
  $scope.theForm = {
    valid: true
};

var paramsDpt = {
    data: {elements: 'all'},
    url: 'get/departamentos'
};

dataService.getData(paramsDpt.data, paramsDpt.url, function(dataResponse){
    if(dataResponse.data.response === true){
        console.log({"respuesta": dataResponse});
        $scope.departamentos = dataResponse.data.departamentos;
    };
});



$scope.getmunicipios = function(params){
    if(typeof params.id != 'undefied'){
        var paramsDpt = {
            data: {elements: 'all'},
            url: 'get/municipios/' + params.id
        };
    };

    dataService.getData(paramsDpt.data, paramsDpt.url, function(dataResponse){
        if(dataResponse.data.response === true){
            $scope.municipios = dataResponse.data.municipios;
        };
    });
};

$scope.saveForm = function(params){
    console.log(params.valid);
    $scope.theForm.valid = params.valid;
    if(params.valid === true){
        var paramsForm = {
            data: $scope.Formulario,
            url: 'set/formulario'
        };

        dataService.setData(paramsForm.data, paramsForm.url, function(dataResponse){
            if(dataResponse.response === true){
                console.log({"Respuesta formulario: ": dataResponse});
                $scope.showSave = true;
                $scope.errorSave = false;
                $scope.errorValid = false;
            }else{
              console.log({"Error al guardar: ": dataResponse});
                $scope.showSave = false;
                $scope.errorSave = true;
                $scope.errorValid = false;
            };
        });

    }else{
      console.log({"Datos no validados: ": $scope.Formulario});
        $scope.showSave = false;
        $scope.errorSave = false;
        $scope.errorValid = true;
    };
};

}]);