<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo Jquery</title>
</head>
<body>
	<h1>
    Jquery
  </h1>
	<a class="btn btn-primary" href="./curso/basicos" role="button">Volver</a>
	<br/><br/>
	<div>
		Ingrese nombre:
		<input type="text" name="nombre" id="nombre"></input>
	</div>
	<div id="output">Hola</div>
</body>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="./assets/js/basicos/jquery.js" rel="stylesheet"></script>
</html>
